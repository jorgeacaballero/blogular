require 'test_helper'

class ShareControllerTest < ActionController::TestCase
  test "should get search" do
    get :search
    assert_response :success
  end

  test "should get share" do
    get :share
    assert_response :success
  end

end
