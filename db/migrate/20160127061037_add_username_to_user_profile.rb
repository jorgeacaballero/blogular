class AddUsernameToUserProfile < ActiveRecord::Migration
  def change
    add_column :user_profiles, :username, :string
  end
end
