class AddFollowerCountToBlog < ActiveRecord::Migration
  def change
    add_column :blogs, :followers_count, :integer, default: 0
  end
end
