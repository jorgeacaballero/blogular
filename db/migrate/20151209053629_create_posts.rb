class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.references :user, index: true, foreign_key: true
      t.references :blog, index: true, foreign_key: true
      t.string :url

      t.timestamps null: false
    end
  end
end
