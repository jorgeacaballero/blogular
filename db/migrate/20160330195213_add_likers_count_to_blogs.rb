class AddLikersCountToBlogs < ActiveRecord::Migration
  def change
    add_column :blogs, :likers_count, :integer, default: 0
  end
end
