class CreateReadStates < ActiveRecord::Migration
  def change
    create_table :read_states do |t|
      t.datetime :last_read, :default => DateTime.now
      t.references :user, index: true, foreign_key: true
      t.references :blog, index: true, foreign_key: true
    end
  end
end
