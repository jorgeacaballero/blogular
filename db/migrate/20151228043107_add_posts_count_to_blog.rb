class AddPostsCountToBlog < ActiveRecord::Migration
  def change
    add_column :blogs, :posts_count, :integer, default: 0
  end
end
