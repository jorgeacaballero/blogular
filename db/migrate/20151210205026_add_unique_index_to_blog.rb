class AddUniqueIndexToBlog < ActiveRecord::Migration
  def change
    add_index :blogs, :feed, unique: true
  end
end
