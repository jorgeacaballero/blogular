source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.4'
# Use postgresql as the database for Active Record
gem 'pg'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Handles authentication
gem 'devise'

# ES6 FTW
gem 'sprockets-es6'

# Discovery System
gem 'feedbag'
gem 'opengraph_parser'

# RSS Parser
gem 'feedjira'

# Handle tricky URLs
gem 'addressable'

# Activity feeds
gem 'public_activity'

# File uploading
gem "paperclip", "~> 4.3"

# Grids are kewl
gem 'neat'

# Handle likes, mentions and follows
gem 'socialization'

# Handle post comments
gem 'acts_as_commentable'

# Handle mentions UI
gem 'jquery-atwho-rails'

# Handle pagination
gem 'will_paginate'

# Serialize stuff to json
gem 'active_model_serializers'

# Handle searching
gem 'pg_search'

# Too lazy to write this again.
gem 'where-or'

# Tagging
gem 'acts-as-taggable-on'

# Http requests + partying
gem 'httparty'

# Puma ftw
gem 'puma'

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
end

