Rails.application.routes.draw do
  devise_for :users

  get 'blog/:id' => 'blog#show', as: :blog
  get 'blog/:id/comments' => 'blog#comments', as: :blog_comments
  post 'blog/:id/comments' => 'blog#comment', as: :blog_comment
  get 'blog/:id/follow' => 'blog#follow', as: :follow_blog
  get 'blog/:id/like' => 'blog#like', as: :like_blog
  get 'blog/:id/visit' => 'blog#visit', as: :visit_blog
  get 'blog/:id/has_new' => 'blog#has_new', as: :has_new

  get 'user/profile', as: :user_profile
  get 'user/profile/edit' => 'user#edit_profile', as: :edit_user_profile
  patch 'user/profile/edit' => 'user#patch_profile'
  get 'user/onboard' => 'user#onboard', as: :onboarding
  patch 'user/onboard' => 'user#patch_onboard'

  get 'user/:id/follow' => 'user#follow', as: :follow_user

  get 'user/likes'
  get 'user/:id/likes' => 'user#likes'

  get 'user/friends'
  get 'user/:id/friends' => 'user#friends'

  get 'user/blogs'
  get 'user/:id/blogs' => 'user#blogs'

  get 'user/:id' => 'user#profile', as: :user
  post 'user/notifications/read' => 'user#read_notifications', as: :read_notifications

  post 'post/:id/comment' => 'post#comment', as: :post_comment
  get 'post/:id' => 'post#show', as: :post
  get 'post/:id/like' => 'post#like', as: :like_post
  get 'post/:id/visit' => 'post#visit', as: :post_visit

  get 'share/scrape', as: :scrapper
  post 'share/share', as: :share
  get 'share/mentionables', as: :mentionables

  get 'feed/discover', as: :discover
  get 'feed/discover/posts' => 'feed#posts', as: :discover_posts
  get 'feed/discover/blogs' => 'feed#blogs', as: :discover_blogs
  get 'feed/discover/users' => 'feed#users', as: :discover_users
  get 'feed/discover/tags' => 'feed#tags', as: :discover_tags

  get 'search' => 'feed#search', as: :search

  root 'feed#home'
end
