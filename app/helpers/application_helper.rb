module ApplicationHelper
  def conditional_label(bool, true_string, false_string)
    if bool
      true_string
    else
      false_string
    end
  end

  def random_color
    ['rgb(255, 211, 95)',
    'rgb(255, 103, 100)',
    'rgb(75, 141, 228)',
    '#8BC34A', '#9C27B0', '#3F51B5', '#009688', '#00BCD4', '#9C27B0'].sample
  end

  def linkify(text)
    text.gsub(/@([a-z\d_]+)/i, '<a href="/user/\1">@\1</a>')
  end
end
