module UserHelper
  def site(url)
    unless url.nil?
      url.sub(/^https?\:\/\//, '').sub!(/^www./,'')
    end
  end
end
