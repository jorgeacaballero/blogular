class FeedController < ApplicationController
  before_action :authenticate_user!

  def home
    @feed = PublicActivity::Activity.where(key: ['user.post.shared'])
                                    .where(trackable_id: current_user.followees(User),  trackable_type: 'User')
                                    .or(PublicActivity::Activity.where(owner_id: current_user.followees(Blog), owner_type: 'Blog'))
                                    .order(created_at: :desc)
                                    .paginate(page: params[:page])
                                    .per_page(10)

    begin
      @blogs = current_user.followees(Blog).sort_by(&:title)
    rescue
      @blogs = current_user.followees(Blog)
    end
  end

  def discover
    @feed = Post.limit(24).order("RANDOM()")
    #@blogs = Blog.order(followers_count: :desc).limit(10)
    @blogs = Blog.limit(10).order("RANDOM()")
    @tags = ActsAsTaggableOn::Tag.most_used(30)
    @users = User.all.where.not(id: current_user.id).order('followers_count DESC').limit(8)
  end

  def blogs
    @blogs = Blog.order(followers_count: :desc).limit(60)
  end

  def posts
    #@feed = Post.order(comments_count: :desc, likers_count: :desc, created_at: :desc).limit(20)
    @feed = Post.limit(40).order("RANDOM()")
  end

  def users
    @users = User.all.where().not(id: current_user.id).order(followers_count: :desc).limit(12)
  end

  def tags
    @tags = ActsAsTaggableOn::Tag.most_used(300)
  end

  def search
    @users = User.search(params[:query])
    @blogs = Blog.search(params[:query]) + Blog.tagged_with(params[:query])
    @posts = Post.search(params[:query]) + Post.tagged_with(params[:query])

    @posts.uniq! { |post| post.id }
  end
end
