class ShareController < ApplicationController
  before_action :authenticate_user!

  def scrape
    @url, @feeds, @og = parseURL
  end

  def share
    url, feeds, og = parseURL

    unless og.images.count
      image = nil
    else
      begin
        image = Addressable::URI.parse(og.images[0]["url"])
      rescue
        image = nil
      end
    end

    blog = Blog.create_for_feed(url: url, feed: feeds[0], title: og.title)

    unless blog.nil?
      blog.tag_list.add(params[:tags], parse: true)
      blog.save!
    end

    post = Post.new(title: og.title, url: url.to_s, user: current_user, blog: blog, image: image.to_s);
    post.tag_list = params[:tags]

    if post.errors[:image].count > 0
      post.image = nil
    end

    image = post.image.queued_for_write[:original]

    unless image.nil?
      geometry = Paperclip::Geometry.from_file(image)

      if geometry.width.to_i < 500 || geometry.width.to_i < 250
        post.image = nil
      end
    end

    if post.save
      @activity = current_user.create_activity action: 'post.shared', parameters: { permissions: ['all'] }, recipient: post, owner: blog

      if params[:follow] && blog.nil? == false
        current_user.follow!(blog)
      end
    end
  end

  def mentionables
    render json: current_user.friends, root: false
  end

  private
  def share_params
    params.permit(:url, :'_', :utf8, :tags, :follow)
  end

  def parseURL
    url = ''
    feeds = ''
    og = ''

    begin
      url = Addressable::URI.parse(params['url'])

      if !url.scheme
        url = 'http://' + url.to_s
      else
        url = url.to_s
      end

      feeds = Feedbag.find(url)

      key = 'a8c46edb5cfa4712930ee20870f7cae9'
      url = Addressable::URI.encode_component(url, Addressable::URI::CharacterClasses::QUERY)

      embedly = "https://api.embedly.com/1/extract?url=#{url}&key=#{key}"

      res = HTTParty.get(embedly)
      og = OpenStruct.new(JSON.parse(res.body))
    rescue URI::InvalidURIError, Addressable::URI::InvalidURIError
      url = false;
    end

    return url, feeds, og
  end
end
