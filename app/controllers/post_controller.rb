class PostController < ApplicationController
  before_action :authenticate_user!

  def show
    @post = Post.find(params[:id])
  end

  def like
    @post = Post.find(params[:id])
    @liked = current_user.toggle_like! @post
  end

  def visit
    @post = Post.find(params[:id])

    unless @post.blog.nil?
      @receipt = ReadState.find_or_initialize_by(user_id: current_user.id, blog_id: @post.blog.id)
      @receipt.last_read = DateTime.now
      @receipt.save
    end

    frameoptions = HTTParty.get(@post.url).headers['x-frame-options'];
    redirect = false

    unless frameoptions.nil?
       if frameoptions == 'SAMEORIGIN' or frameoptions == 'sameorigin'
          redirect = true
        end
    end

    unless redirect
      render 'post/visit', layout: false
    else
      redirect_to @post.url
    end


  end

  def comment
    @post = Post.find(params[:id])
    comment = @post.comments.create
    comment.comment = params[:comment]
    comment.user = current_user
    comment.save
    @comment = comment

    unless params[:mentions].nil?
      params[:mentions].each do |mention|
        user = User.find(mention)
        @comment.mention!(user)
        user.create_activity action: 'mention', recipient: @comment, owner: current_user
      end
    end
  end
end
