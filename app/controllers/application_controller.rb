class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :set_notifications
  before_action :onboarding
  layout :layout_by_resource

  def layout_by_resource
    if devise_controller?
      'devise'
    else
      'application'
    end
  end

  def onboarding
    if current_user.nil?
      return
    end

    if params[:action] == 'onboard' || params[:action] == 'patch_onboard'
      return;
    end

    if current_user.profile.name.blank? || current_user.profile.username.blank?
      redirect_to onboarding_path
    end

  end

  def set_notifications
    unless current_user.nil?
      @notifications = PublicActivity::Activity.where(key: ['user.mention', 'user.follow'], trackable_id: current_user.id, trackable_type: 'User').order(created_at: :desc).limit(8)
    end
  end
end
