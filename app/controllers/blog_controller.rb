class BlogController < ApplicationController
  before_filter :get_blog
  before_action :authenticate_user!

  def show
    @posts = @blog.posts.order(created_at: :desc)
                        .paginate(page: params[:page])
                        .per_page(6)
  end

  def comments
  end

  def comment
    comment = @blog.comments.create
    comment.comment = params[:comment]
    comment.user = current_user
    comment.save
    @comment = comment

    unless params[:mentions].nil?
      params[:mentions].each do |mention|
        user = User.find(mention)
        @comment.mention!(user)
        user.create_activity action: 'mention', recipient: @comment, owner: current_user
      end
    end
  end

  def follow
    @following = current_user.toggle_follow!(@blog)
  end

  def like
    @liked = current_user.toggle_like! @blog
  end

  def visit
    @blog = Blog.find(params[:id])
    frameoptions = HTTParty.get(@blog.url).headers['x-frame-options'];
    redirect = false

    unless frameoptions.nil?
       if frameoptions == 'SAMEORIGIN' or frameoptions == 'sameorigin'
          redirect = true
        end
    end

    unless @blog.nil?
      @receipt = ReadState.find_or_initialize_by(user_id: current_user.id, blog_id: @blog.id)
      @receipt.last_read = DateTime.now
      @receipt.save
    end

    unless redirect
      render 'blog/visit', layout: false
    else
      redirect_to @blog.url
    end
  end

  def has_new
    @blog = Blog.find(params[:id])
    render json: { has_new: @blog.new_posts?(current_user) }
  end

  private
  def get_blog
    @blog = Blog.find(params[:id])
  end
end
