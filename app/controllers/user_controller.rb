class UserController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user
  before_action :edit_profile_if_empty, only: [:blogs, :friends, :profile, :likes]

  def profile
    @posts = @user.posts.order("created_at desc")
                        .paginate(page: params[:page])
                        .per_page(4)
  end

  def blogs
    @blogs = @user.followees(Blog).sort_by(&:title)
  end

  def likes
    @like_ids = Like.where(liker_id: current_user.id, likeable_type: 'Post').map(&:likeable_id)

    @likes = Post.where(id: @like_ids).paginate(page: params[:page]).per_page(4)
  end

  def friends
    @friends = @user.friends
  end

  def follow
    @following = current_user.toggle_follow!(@user)

    if @following
      @user.create_activity action: 'follow', recipient: @user, owner: current_user
    end
  end

  def edit_profile
    @profile = current_user.profile
  end

  def patch_profile
    @profile = current_user.profile
    if @profile.update_attributes(user_profile)
      redirect_to edit_user_profile_path, flash: { success: 'You profile was updated successfully.' }
    else
      flash[:alert] = 'Check the form, some fields were not filled correctly.'
      render :edit_profile
    end
  end

  def read_notifications
    PublicActivity::Activity.where( key: ['user.mention', 'user.follow'],
                                    trackable_id: current_user.id,
                                    trackable_type: 'User',
                                    read: false).update_all(read: true)

    render nothing: true, status: :ok
  end

  def onboard
    @profile = current_user.profile
  end

  def patch_onboard
    @profile = current_user.profile
    if @profile.update_attributes(user_profile)
      redirect_to root_path, flash: {
        success: 'All Set! Welcome aboard!'
      }
    else
      render :onboard
    end
  end

  private
  def user_profile
    params.require(:user_profile).permit(:avatar, :name, :username, :website, :bio, :location)
  end

  def set_user
    if params[:id].nil?
      @user = current_user
    else
      if params[:id].to_i != 0
        @user = User.find(params[:id])
      else
        @user = UserProfile.where(username: params[:id].downcase).first.user
      end
    end

    @profile = @user.profile
  end

  def edit_profile_if_empty
    if @user == current_user && @profile.name.blank?
      redirect_to edit_user_profile_path, flash: { notice: 'Your profile is empty. Please fill it in.' }
    end
  end
end
