class UserSerializer < ActiveModel::Serializer 
  attributes :id, :name, :username

  def username
    object.profile.username
  end

  def name
    object.profile.name
  end
end