class User < ActiveRecord::Base
  include PublicActivity::Common
  include PgSearch

  acts_as_liker
  acts_as_follower
  acts_as_followable
  acts_as_mentionable
  
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :posts, dependent: :delete_all
  has_one :profile, dependent: :delete, class_name: 'UserProfile'
  has_one :read_state, dependent: :delete

  after_create :create_profile
  after_create :follow_thyself

  pg_search_scope :search, 
                  against: :email,
                  associated_against: {
                    profile: [:name]
                  }, 
                  using: {
                    tsearch: { prefix: true }, 
                    trigram: {}
                  }

  def follow_thyself
    self.follow!(self)
  end

  def create_profile
    UserProfile.new(user: self).save(validate: false)
  end

  def friends
    followees = self.followees(User)
    followers = self.followers(User)

    friends = followees + followers
    friends.uniq!
    friends.delete_if { |user| user.id == self.id }

    return friends
  end
end
