class UserProfile < ActiveRecord::Base
  belongs_to :user
  has_attached_file :avatar, styles: { medium: '300x300#', thumb: '64x64#' }, default_url: 'user.png'
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/
  validates :username, :uniqueness => {:case_sensitive => false}
  validates :username, :name, presence: true

  before_save :validate_username

  def validate_username
    unless self.username.nil?
      self.username = self.username.downcase
    end
  end
end
