class Blog < ActiveRecord::Base
  include PublicActivity::Common
  include PgSearch

  has_many :posts
  has_one :read_state, dependent: :delete

  acts_as_followable
  acts_as_likeable
  acts_as_commentable
  acts_as_taggable

  pg_search_scope :search,
                  against: [:title],
                  using: {
                    tsearch: { prefix: true },
                    trigram: {}
                  }


  def self.create_for_feed(data)
    if data[:feed].nil?
      return nil
    end

    blog = Blog.where(feed: data[:feed])

    if blog.length > 0
      return blog.first
    end

    site = Addressable::URI.parse(data[:url]).normalized_site.to_s

    begin
      feed = Feedjira::Feed.fetch_and_parse data[:feed]
    rescue
      begin
        feed = Feedjira::Feed.fetch_and_parse data[:url]
      rescue
        feed = false
      end
    end

    blog = Blog.new
    blog.feed = data[:feed]
    if feed
      blog.title = feed.title ? feed.title : data[:title]
    else
      blog.title = data[:title] ?  data[:title] : "[No Name]"
    end
    blog.url = site

    blog.save!

    return blog
  end

  def new_posts?(user)
    receipt = ReadState.where(blog: self, user: user)

    if receipt.count > 0
      time = receipt.first.last_read
    else
      return true
    end

    begin
      feed = Feedjira::Feed.fetch_and_parse(self.feed)
    rescue
      return false
    end

    latest = feed.entries.first

    if latest.nil?
      return false
    end

    return latest.published > time
  end
end
