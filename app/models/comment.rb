class Comment < ActiveRecord::Base

  include ActsAsCommentable::Comment

  acts_as_mentioner

  belongs_to :commentable, :polymorphic => true, counter_cache: true

  default_scope -> { order('created_at ASC') }

  belongs_to :user
end
