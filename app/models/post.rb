class Post < ActiveRecord::Base
  include PublicActivity::Common
  include PgSearch

  acts_as_likeable
  acts_as_commentable
  acts_as_taggable

  pg_search_scope :search, 
                  against: [:title], 
                  using: {
                    tsearch: { prefix: true }, 
                    trigram: {}
                  }

  has_attached_file :image, styles: { large: "600x300#", medium: "300x150#" }
  validates_attachment_content_type :image, content_type: ['image/jpg', 'image/jpeg', 'image/png', 'image/gif']
  
  belongs_to :user
  belongs_to :blog, counter_cache: true
end
