const ready = () => {
	$('[data-blog] .comments textarea').on('focus', function() {
		$(this).parents('form').addClass('expanded');
	}).on('blur', function() {
		let $this = $(this);

		if ($this.val().length === 0) {
			$(this).parent().removeClass('expanded');
		}
	});

	$('.sidebar .blogs li').on('click', function() {
		$(this).find('span').fadeOut();
	});

	$('.blogs-feed .title a').on('click', function() {
		$(this).parent().find('.status').fadeOut();
	});
};

const get_new = () => {
  let blogs = $('.following__tags__').find('li');
  $.each(blogs, (index, element) => {
    let tag = $(element);
    $.ajax({
      type: "GET",
      url: `/blog/${tag.data("id")}/has_new`,
      success: function(data){
        if (data.has_new){
          let span = tag.find('span');
          span.fadeIn("slow");
        }
      }
    })
  });

  let blogs_following = $('.blog__tags__').find('li');
  $.each(blogs_following, (index, element) => {
    let tag = $(element);
    $.ajax({
      type: "GET",
      url: `/blog/${tag.data("id")}/has_new`,
      success: function(data){
        if (data.has_new){
          let span = tag.find('.status');
          span.fadeIn("slow");
        }
      }
    })
  });
};

$(document).ready(ready);
$(document).ready(get_new);
$(document).on('page:load', ready);
$(document).on('page:load', get_new);

