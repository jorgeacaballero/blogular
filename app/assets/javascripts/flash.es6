$(function() {
	$(document).on('page:update', () => {
		$('.flash').not('[data-sticky]').delay(2000).fadeOut(function() {
			$(this).remove();
		});

		$('.flash').on('click', function() {
			$(this).remove();
		});
	});
});