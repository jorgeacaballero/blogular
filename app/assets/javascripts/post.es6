const ready = () => {
	$('[data-post] .comments textarea').on('focus', function() {
		$(this).parents('form').addClass('expanded');
	}).on('blur', function() {
		let $this = $(this);

		if ($this.val().length === 0) {
			$(this).parent().removeClass('expanded');
		}
	});
};

$(document).ready(ready);
$(document).on('page:update', ready);