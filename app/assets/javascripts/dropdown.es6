class Dropdown {
	constructor($el) {
		this.$el = $el;
		this.menu = $('#' + this.$el.data('dropdown'));
		this.menu.hide();

		this.$el.off('click.menu').on('click.menu', () => this.openMenu());
	}

	openMenu() {
		if (this.menu.is(':visible')) {
			this.menu.hide();
		} else {
			this.menu.show();
		}
	}
}

$(function() {
	$(document).on('page:update', () => {
		var menu = $('[data-dropdown]');
		if (menu.length) {
			new Dropdown(menu);
		}
	});
});