$(function() {
	$(document).on('page:update', () => {
		if ($('[data-infinity]').length) {
			$('.activity-feed').jscroll({
				nextSelector: 'a.next_page',
				contentSelector: '.activity-feed > li, .pagination',
				pagingSelector: '.pagination',
				callback: function() {
					$('.pagination').hide();
				}
			});
		}
	});
});