class LinkActivity {
	constructor(activity) {
		this.activity = $(activity);

		let img = this.activity.data('image');
		if (img.indexOf('missing') === -1) {
			this.activity.css('background-image', `url(${img})`);
			this.activity.addClass('expanded');
		}
	}
}

$(function() {
	$(document).on('page:update', () => {
		let $activities = $('[data-activity]');
		$activities.each(function(i, activity) {
			new LinkActivity(activity);
		});
	});
});